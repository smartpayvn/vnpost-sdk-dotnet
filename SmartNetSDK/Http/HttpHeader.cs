﻿namespace SmartNet.Http
{
    public sealed class HttpHeader
    {
        public const string XRequestID = "X-Request-ID";
        public const string XPartnerCode = "X-Partner-Code";
        public const string XSignature = "X-Signature";

        private HttpHeader()
        {
        }
    }
}
