﻿namespace SmartNet.Http
{
    public sealed class ContentType
    {
        public const string Json = "application/json";

        private ContentType()
        {
        }
    }
}
