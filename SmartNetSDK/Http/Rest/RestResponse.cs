﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SmartNet.SmartPay.Gateway;
using SmartNet.Util;

namespace SmartNet.Http.Rest
{
    public sealed class RestResponse
    {
        private ResultCode resultCode = ResultCode.CallingFailed;
        private HttpStatusCode httpCode;
        private WebHeaderCollection headers;
        private string contentType;
        private string body;

        internal RestResponse()
        {
        }

        public ResultCode RetCode()
        {
            return this.resultCode;
        }

        public RestResponse RetCode(ResultCode value)
        {
            this.resultCode = value;
            return this;
        }

        public HttpStatusCode HttpCode()
        {
            return this.httpCode;
        }

        public RestResponse HttpCode(HttpStatusCode value)
        {
            this.httpCode = value;
            if (HttpStatusCode.OK == value || HttpStatusCode.NoContent == value)
            {
                this.resultCode = ResultCode.Success;
            }
            else
            {
                this.resultCode = ResultCode.Fail;
            }


            return this;
        }

        public RestResponse Body(string contentType, string charset, Stream responseStream)
        {
            if (null == responseStream)
            {
                System.Console.WriteLine("responseStream=null");
                return this;
            }

            Encoding encoding = DynamicDataContainer.Utf8;
            if (null != charset)
            {
                encoding = Encoding.GetEncoding(charset);

            }

            if (null != contentType && contentType.Contains('='))
            {
                int splitIndex = contentType.IndexOf('=');
                encoding = Encoding.GetEncoding(contentType.Substring(splitIndex + 1));
                contentType = contentType.Substring(0, contentType.IndexOf(';'));
            }

            using (StreamReader sr = new StreamReader(responseStream, encoding))
            {
                this.body = sr.ReadToEnd();
            }

            this.contentType = contentType;

            System.Console.WriteLine("charset=" + charset);
            System.Console.WriteLine("contentType=" + contentType);

            return this;
        }

        public string Body()
        {
            return this.body;
        }

        public JObject BodyAsJObject()
        {
            if (StringUtil.IsNullOrEmpty(this.body))
            {
                return null;
            }

            if (!ContentType.Json.Equals(this.contentType))
            {
                throw new SmartNetException("Unsupported Content-Type");
            }

            return JObject.Parse(this.body);
        }

        public RestResponse Headers(WebHeaderCollection headers)
        {
            this.headers = headers;
            return this;
        }

        public string Header(string name)
        {
            if (this.headers == null)
            {
                return null;
            }

            return this.headers.Get(name);
        }
    }
}
