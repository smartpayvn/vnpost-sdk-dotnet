﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
namespace SmartNet.Http.Rest
{
    public sealed class RestRequest
    {
        private readonly string url;
        private readonly HttpWebRequest httpRequest;

        private byte[] body;

        public RestRequest(string url)
        {
            this.url = url;
            this.httpRequest = WebRequest.CreateHttp(url);
        }

        public RestRequest Method(string httpMethod)
        {
            this.httpRequest.Method = httpMethod;
            return this;
        }

        public RestRequest Body(string contentType, Encoding encoding, String requestBody)
        {
            return this.Body(this.AddCharset(contentType, encoding), encoding.GetBytes(requestBody));
        }

        public RestRequest Body(string contentType, byte[] requestBody)
        {
            this.httpRequest.ContentType = contentType;
            this.body = requestBody;
            return this;
        }

        public RestRequest Header(string headerName, string headerValue)
        {
            this.httpRequest.Headers.Add(headerName, headerValue);
            return this;
        }

        public RestRequest Headers(IDictionary<string, string> headers)
        {
            foreach (KeyValuePair<string, string> header in headers)
            {
                this.httpRequest.Headers.Add(header.Key, header.Value);
            }

            return this;
        }

        public RestResponse Send()
        {
            Console.WriteLine("url=" + this.url);

            if (null != this.body)
            {
                this.httpRequest.ContentLength = body.Length;
                this.httpRequest.GetRequestStream()
                    .Write(body, 0, body.Length);
            }

            RestResponse response = new RestResponse();
            HttpWebResponse httpResponse;
            try
            {
                httpResponse = this.httpRequest.GetResponse() as HttpWebResponse;
                Console.WriteLine();
            }
            catch (System.Net.WebException httpEx)
            {
                Console.WriteLine();
                Console.WriteLine(httpEx);
                Console.WriteLine();

                httpResponse = httpEx.Response as HttpWebResponse;
            }

            response.HttpCode(httpResponse.StatusCode)
                .Headers(httpResponse.Headers)
                .Body(httpResponse.ContentType, httpResponse.CharacterSet, httpResponse.GetResponseStream());

            return response;
        }

        private string AddCharset(String contentType, Encoding encoding)
        {
            if (null == contentType)
            {
                return null;
            }

            if (null == encoding)
            {
                return contentType;
            }

            if (contentType.Contains("charset="))
            {
                return contentType;
            }

            return contentType + ";charset=" + encoding.BodyName.ToUpper();
        }
    }
}
