﻿namespace SmartNet.Http
{
    public sealed class HttpMethod
    {
        public const string Post = "POST";
        public const string Get = "GET";
        public const string Put = "PUT";
        public const string Delete = "DELETE";

        private HttpMethod()
        {
        }
    }
}
