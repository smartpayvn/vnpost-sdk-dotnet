﻿namespace SmartNet
{
    public enum ResultCode
    {
        Success,
        Fail,
        CallingFailed,
        SystemError,
    }
}
