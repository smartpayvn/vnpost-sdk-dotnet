﻿using System;
namespace SmartNet.Util
{
    public sealed class DateTimeUtil
    {
        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        private DateTimeUtil()
        {
        }

        public static string NowAsStr() => DateTime.Now.ToString(DateTimeFormat);
    }
}
