﻿namespace SmartNet.Util
{
    public sealed class StringUtil
    {
        private StringUtil()
        {
        }

        public static bool IsNullOrEmpty(string str)
        {
            return null == str || str.Trim().Length == 0;
        }
    }
}
