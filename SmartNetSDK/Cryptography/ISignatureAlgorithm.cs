﻿namespace SmartNet.Cryptography
{
    public interface ISignatureAlgorithm
    {
        byte[] Sign(byte[] data);
        bool Verify(byte[] data, byte[] signature);
    }
}
