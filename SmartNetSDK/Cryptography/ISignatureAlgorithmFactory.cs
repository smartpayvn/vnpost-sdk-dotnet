﻿namespace SmartNet.Cryptography
{
    public interface ISignatureAlgorithmFactory
    {
        ISignatureAlgorithm Signer();

        ISignatureAlgorithm Verifier();
    }
}
