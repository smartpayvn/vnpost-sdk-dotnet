﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace SmartNet.Cryptography
{
    public sealed class RSAWithSHA256 : ISignatureAlgorithm
    {
        private readonly RSA rsa;

        private RSAWithSHA256(RSA rsa)
        {
            this.rsa = rsa;
        }

        public static RSAWithSHA256 FromPfxData(byte[] pfxData, String password)
        {
            X509Certificate2 pfx = new X509Certificate2(pfxData, password, X509KeyStorageFlags.Exportable);
            if (!pfx.HasPrivateKey)
            {
                return null;
            }

            RSA rsa = pfx.GetRSAPrivateKey();
            if (null == rsa)
            {
                return null;
            }

            return new RSAWithSHA256(rsa);
        }

        public static RSAWithSHA256 FromCertData(byte[] certData)
        {
            X509Certificate2 crt = new X509Certificate2(certData);

            RSA rsa = crt.GetRSAPublicKey();
            if (null == rsa)
            {
                return null;
            }

            return new RSAWithSHA256(rsa);
        }

        public byte[] Sign(byte[] data)
        {
            return this.rsa.SignData(data, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
        }

        public bool Verify(byte[] data, byte[] signature)
        {
            return this.rsa.VerifyData(data, signature, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
        }
    }
}
