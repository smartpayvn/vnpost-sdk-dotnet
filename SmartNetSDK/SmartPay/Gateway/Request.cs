﻿using System;
using SmartNet.Cryptography;

namespace SmartNet.SmartPay.Gateway
{
    public abstract class Request<T> : DynamicDataContainer where T : Response
    {
        private readonly string id;
        private readonly string callerCode;

        protected Request(ISignatureAlgorithm signatureSigner, String callerCode, String requestId)
            : base(signatureSigner)
        {
            this.id = requestId;
            this.callerCode = callerCode;

            Console.WriteLine("callerCode=" + callerCode);
            Console.WriteLine("requestId=" + requestId);
        }

        public abstract T Send();
    }
}
