﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using SmartNet.Cryptography;

namespace SmartNet.SmartPay.Gateway
{
    public abstract class DynamicDataContainer
    {
        public static readonly Encoding Utf8 = new UTF8Encoding();

        private readonly JObject data;
        private readonly ISignatureAlgorithm signatureSigner;
        private readonly string signature;
        private readonly ISignatureAlgorithm signatureVerifier;

        protected DynamicDataContainer(ISignatureAlgorithm signatureSigner)
        {
            this.data = new JObject();
            this.signatureSigner = signatureSigner;
        }

        protected DynamicDataContainer(JObject data, string signature, ISignatureAlgorithm signatureVerifier)
        {
            this.data = data;
            this.signature = signature;
            this.signatureVerifier = signatureVerifier;
        }

        protected void Add(string name, object value)
        {
            this.data.Add(name, new JValue(value));
        }

        protected string GetString(string name)
        {
            if (!this.data.ContainsKey(name))
            {
                return null;
            }

            return this.data.GetValue(name).Value<string>();
        }

        protected int GetInt(string name)
        {
            if (!this.data.ContainsKey(name))
            {
                throw new SmartNetException("Field '" + name + "' not found");
            }

            return this.data.GetValue(name).Value<int>();
        }

        protected byte[] GenerateSignature()
        {
            if (null == this.signatureSigner)
            {
                return null;
            }

            string rawData = this.AsString();
            System.Console.WriteLine("rawData=" + rawData);

            return this.signatureSigner.Sign(Utf8.GetBytes(rawData));
        }

        protected bool VerifySignature()
        {
            if (null == this.signature)
            {
                return false;
            }

            if (null == this.signatureVerifier)
            {
                return false;
            }

            string rawData = this.AsString();
            System.Console.WriteLine("rawData=" + rawData);

            return this.signatureVerifier.Verify(Utf8.GetBytes(rawData),
                Convert.FromBase64String(this.signature)
                );
        }

        protected string AsJsonString()
        {
            return this.data.ToString();
        }

        protected virtual string AsString()
        {
            if (null != this.data && this.data.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                IDictionary<string, JToken> sortedDic = new SortedDictionary<string, JToken>(this.data);
                foreach (JToken value in sortedDic.Values)
                {
                    sb.Append(value.Value<string>())
                        .Append('|');
                }

                return sb.ToString(0, sb.Length - 1);
            }

            return null;
        }
    }
}
