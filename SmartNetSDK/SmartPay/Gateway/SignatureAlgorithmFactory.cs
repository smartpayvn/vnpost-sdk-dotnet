﻿using System.Linq;
using System.IO;
using SmartNet.Cryptography;
using System;

namespace SmartNet.SmartPay.Gateway
{
    public class SignatureAlgorithmFactory : ISignatureAlgorithmFactory
    {
        private readonly byte[] pfxData;
        private readonly byte[] certData;
        private readonly string pfxPassword;

        public SignatureAlgorithmFactory(string pfxFilePath, string pfxPassword, string smartnetCertFilePath)
        {
            this.pfxData = File.ReadAllBytes(pfxFilePath);
            this.certData = File.ReadAllBytes(smartnetCertFilePath);
            this.pfxPassword = pfxPassword;
        }

        public ISignatureAlgorithm Signer()
        {
            return RSAWithSHA256.FromPfxData(this.pfxData.ToArray<byte>(), this.pfxPassword);
        }

        public ISignatureAlgorithm Verifier()
        {
            return RSAWithSHA256.FromCertData(this.certData.ToArray<byte>());
        }
    }
}
