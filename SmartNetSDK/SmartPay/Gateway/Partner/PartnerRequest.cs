﻿using SmartNet.Cryptography;

namespace SmartNet.SmartPay.Gateway.Partner
{
    public abstract class PartnerRequest<T> : Request<T> where T : PartnerResponse
    {
        private const string AgentIdField = "agentId";

        protected PartnerRequest(ISignatureAlgorithm signatureSigner, string partnerCode, string requestId, string agentId)
            : base(signatureSigner, partnerCode, requestId)
        {
            base.Add(AgentIdField, agentId);
        }
    }
}
