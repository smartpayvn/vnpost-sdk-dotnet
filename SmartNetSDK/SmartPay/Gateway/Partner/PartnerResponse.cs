﻿using Newtonsoft.Json.Linq;
using SmartNet.Cryptography;

namespace SmartNet.SmartPay.Gateway.Partner
{
    public abstract class PartnerResponse : Response
    {
        protected PartnerResponse(JObject data, string signature, ISignatureAlgorithm signatureVerifier)
            : base(data, signature, signatureVerifier)
        {
        }
    }
}
