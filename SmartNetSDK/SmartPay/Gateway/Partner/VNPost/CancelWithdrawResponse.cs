﻿using SmartNet.Http.Rest;

namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class CancelWithdrawResponse : VNPostResponse
    {
        protected CancelWithdrawResponse(RestResponse restResponse)
            : base(CancelWithdrawRequest.ApiName, restResponse)
        {
        }
    }
}
