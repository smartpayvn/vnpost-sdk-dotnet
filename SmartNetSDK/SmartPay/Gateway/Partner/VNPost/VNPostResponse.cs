﻿using System;
using System.Net;
using System.Text;
using SmartNet.Http;
using SmartNet.Http.Rest;
using SmartNet.SmartPay.PartnerGateway.Client;

namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class VNPostResponse : PartnerResponse
    {
        public const string AmountField = "amount";
        public const string FeeField = "fee";
        public const string PhoneField = "phone";
        public const string NameField = "name";
        public const string NationalIdField = "nationalId";
        public const string TypeField = "type";
        public const string RefNoField = "refNo";

        private readonly string apiName;
        private readonly ResultCode retCode;
        private readonly HttpStatusCode httpCode;

        protected VNPostResponse(string apiName, RestResponse restResponse)
            : base(restResponse.BodyAsJObject(),
                  restResponse.Header(HttpHeader.XSignature),
                  Program.SAF().Verifier())
        {
            this.apiName = apiName;
            this.httpCode = restResponse.HttpCode();
            this.retCode = restResponse.RetCode();
        }

        public sealed override ResultCode RetCode()
        {
            return this.retCode;
        }

        public sealed override ResultCode Validate()
        {
            if (!this.VerifySignature())
            {
                Console.WriteLine("Invalid response signature");
                return ResultCode.SystemError;
            }

            return this.ValidateRequiredFields();
        }

        protected virtual ResultCode ValidateRequiredFields()
        {
            return ResultCode.Success;
        }

        protected override string AsString()
        {
            StringBuilder sb = new StringBuilder()
                .Append(this.apiName)
                .Append('|').Append((int)this.httpCode);

            string baseValue = base.AsString();
            if (null != baseValue)
            {
                sb.Append('|').Append(baseValue);
            }

            return sb.ToString();
        }
    }
}
