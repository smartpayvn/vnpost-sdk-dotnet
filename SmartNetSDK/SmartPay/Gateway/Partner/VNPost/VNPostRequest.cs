﻿using System;
using System.Reflection;
using SmartNet.Http;
using SmartNet.Http.Rest;
using SmartNet.SmartPay.PartnerGateway.Client;
using SmartNet.Util;

namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class VNPostRequest<T> : PartnerRequest<T> where T : VNPostResponse
    {
        protected const string ApiVersion = "v1.0";

        private const string TypeField = "type";
        private const string CodeField = "code";
        private const string TimeField = "time";

        private RestRequest restRequest;
        private readonly string apiName;

        public VNPostRequest(string apiName, string apiVersion,
            string requestId, string agentId, string type, string code)
            : base(Program.SAF().Signer(), Program.PartnerCode, requestId, agentId)
        {
            base.Add(TypeField, type);
            base.Add(CodeField, code);

            this.restRequest = new RestRequest(Program.SmartNetBaseURL + apiVersion + "/" + apiName)
                .Header(HttpHeader.XRequestID, requestId)
                .Header(HttpHeader.XPartnerCode, Program.PartnerCode);

            this.apiName = apiName;
        }

        public override T Send()
        {
            base.Add(TimeField, DateTimeUtil.NowAsStr());

            string requestBody = base.AsJsonString();
            Console.WriteLine("requestBody=" + requestBody);

            string signature = Convert.ToBase64String(base.GenerateSignature());
            Console.WriteLine("signature=" + signature);

            RestResponse restResponse = this.restRequest
                    .Header(HttpHeader.XSignature, signature)
                    .Method(HttpMethod.Post)
                    .Body(ContentType.Json, Utf8, requestBody)
                    .Send();

            return Activator.CreateInstance
                (typeof(T),
                BindingFlags.NonPublic | BindingFlags.Instance,
                null,
                new object[] { restResponse },
                null
                ) as T;
        }

        protected override string AsString()
        {
            return this.apiName + '|' + base.AsString();
        }
    }
}
