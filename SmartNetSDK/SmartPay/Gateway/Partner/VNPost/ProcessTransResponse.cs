﻿using SmartNet.Http.Rest;

namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class ProcessTransResponse : VNPostResponse
    {
        protected ProcessTransResponse(RestResponse restResponse)
            : base(ProcessTransRequest.ApiName, restResponse)
        {
        }

        public string RefNo()
        {
            return base.GetString(RefNoField);
        }
    }
}
