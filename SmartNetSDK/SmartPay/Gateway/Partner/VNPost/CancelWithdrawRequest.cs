﻿namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class CancelWithdrawRequest : VNPostRequest<CancelWithdrawResponse>
    {
        internal const string ApiName = "cancelConfirmation";

        public CancelWithdrawRequest(string requestId, string agentId, string type, string code)
            : base(ApiName, ApiVersion, requestId, agentId, type, code)
        {
        }
    }
}
