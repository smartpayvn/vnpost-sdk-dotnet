﻿using System;
using SmartNet.Http.Rest;

namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class GetTransResponse : VNPostResponse
    {
        protected GetTransResponse(RestResponse restResponse)
            : base(GetTransRequest.ApiName, restResponse)
        {
        }

        public string Type()
        {
            return base.GetString(TypeField);
        }

        public string CustomerPhone()
        {
            return base.GetString(PhoneField);
        }

        public int Amount()
        {
            return base.GetInt(AmountField);
        }

        public int Fee()
        {
            try
            {
                return base.GetInt(FeeField);
            }
            catch (SmartNetException ex)
            {
                return 0;
            }
        }

        public string CustomerName()
        {
            return base.GetString(NameField);
        }

        public string CustomerNationalId()
        {
            return base.GetString(NationalIdField);
        }
    }
}
