﻿namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class ProcessTransRequest : VNPostRequest<ProcessTransResponse>
    {
        internal const string ApiName = "processTrans";

        public ProcessTransRequest(string requestId, string agentId, string type, string code)
            : base(ApiName, ApiVersion, requestId, agentId, type, code)
        {
        }
    }
}
