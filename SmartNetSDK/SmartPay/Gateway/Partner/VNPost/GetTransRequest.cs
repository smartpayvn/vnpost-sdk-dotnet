﻿namespace SmartNet.SmartPay.Gateway.Partner.VNPost
{
    public class GetTransRequest : VNPostRequest<GetTransResponse>
    {
        internal const string ApiName = "getTrans";

        public GetTransRequest(string requestId, string agentId, string type, string code)
            : base(ApiName, ApiVersion, requestId, agentId, type, code)
        {
        }
    }
}
