﻿using Newtonsoft.Json.Linq;
using SmartNet.Cryptography;

namespace SmartNet.SmartPay.Gateway
{
    public abstract class Response : DynamicDataContainer
    {
        protected Response(JObject data, string signature, ISignatureAlgorithm signatureVerifier)
            : base(data, signature, signatureVerifier) 
        {
        }

        public abstract ResultCode Validate();

        public abstract ResultCode RetCode();
    }
}
