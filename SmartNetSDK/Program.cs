﻿using System.Text;
using SmartNet.Cryptography;
using SmartNet.SmartPay.Gateway;
using SmartNet.SmartPay.Gateway.Partner.VNPost;

namespace SmartNet.SmartPay.PartnerGateway.Client
{
    class Program
    {
        public const string SmartNetBaseURL = "https://test-vnpost.paysmart.com.vn/";
        public const string PartnerCode = "VNPOST";

        private const string PfxFilePath = @"../../../../vnpost.pfx";
        private const string PfxPassword = "123456";
        private const string SmartNetCertFilePath = @"../../../../smartnet.crt";

        private static readonly ISignatureAlgorithmFactory SignatureAlgorithmFactory
            = new SignatureAlgorithmFactory(PfxFilePath, PfxPassword, SmartNetCertFilePath);

        public static ISignatureAlgorithmFactory SAF()
        {
            return Program.SignatureAlgorithmFactory;
        }

        static void testDeposit(string requestId, string agentId, string code)
        {
            string type = "deposit";
            GetTransRequest getDepositRequest = new GetTransRequest(requestId, agentId, type, code);
            GetTransResponse resp = getDepositRequest.Send();
            ResultCode validateRespRetCode = resp.Validate();
            System.Console.WriteLine("validateRespRetCode=" + validateRespRetCode);
            if (ResultCode.Success == validateRespRetCode)
            {
                System.Console.WriteLine("retCode=" + resp.RetCode());
                if (ResultCode.Success == resp.RetCode())
                {
                    System.Console.WriteLine("Type=" + resp.Type());
                    System.Console.WriteLine("CustomerPhone=" + resp.CustomerPhone());
                    System.Console.WriteLine("Amount=" + resp.Amount());
                    System.Console.WriteLine("Fee=" + resp.Fee());

                    if (null != resp.CustomerName())
                    {
                        System.Console.WriteLine("CustomerName=" + resp.CustomerName());
                    }

                    if (null != resp.CustomerNationalId())
                    {
                        System.Console.WriteLine("CustomerNationalId=" + resp.CustomerNationalId());
                    }

                    ProcessTransRequest processDepositRequest = new ProcessTransRequest(requestId, agentId, type, code);
                    ProcessTransResponse processTransResponse = processDepositRequest.Send();
                    validateRespRetCode = processTransResponse.Validate();
                    System.Console.WriteLine("validateRespRetCode=" + validateRespRetCode);
                    if (ResultCode.Success == validateRespRetCode)
                    {
                        System.Console.WriteLine("RefNo=" + processTransResponse.RefNo());
                    }
                }
            }
        }

        static void testWithdraw(string requestId, string agentId, string code)
        {
            string type = "withdraw";
            GetTransRequest getWithdrawRequest = new GetTransRequest(requestId, agentId, type, code);
            GetTransResponse getTransResp = getWithdrawRequest.Send();
            ResultCode validateRespRetCode = getTransResp.Validate();
            System.Console.WriteLine("validateRespRetCode=" + validateRespRetCode);
            if (ResultCode.Success == validateRespRetCode)
            {
                System.Console.WriteLine("retCode=" + getTransResp.RetCode());
                if (ResultCode.Success == getTransResp.RetCode())
                {
                    System.Console.WriteLine("Type=" + getTransResp.Type());
                    System.Console.WriteLine("CustomerPhone=" + getTransResp.CustomerPhone());
                    System.Console.WriteLine("Amount=" + getTransResp.Amount());
                    System.Console.WriteLine("CustomerName=" + getTransResp.CustomerName());
                    System.Console.WriteLine("CustomerNationalId=" + getTransResp.CustomerNationalId());

                    ProcessTransRequest processDepositReq = new ProcessTransRequest(requestId, agentId, type, code);
                    ProcessTransResponse processTransResp = processDepositReq.Send();
                    validateRespRetCode = processTransResp.Validate();
                    System.Console.WriteLine("validateRespRetCode=" + validateRespRetCode);
                    if (ResultCode.Success == validateRespRetCode)
                    {
                        System.Console.WriteLine("RefNo=" + processTransResp.RefNo());

                        System.Console.WriteLine("retCode=" + processTransResp.RetCode());
                        if (ResultCode.Success == processTransResp.RetCode())
                        {
                            CancelWithdrawRequest cancelWithdrawReq
                                = new CancelWithdrawRequest(requestId, agentId, type, code);

                            CancelWithdrawResponse cancelWithdrawResp = cancelWithdrawReq.Send();
                            validateRespRetCode = cancelWithdrawResp.Validate();
                            System.Console.WriteLine("validateRespRetCode=" + validateRespRetCode);
                            if (ResultCode.Success == validateRespRetCode)
                            {
                                System.Console.WriteLine("retCode=" + cancelWithdrawResp.RetCode());
                            }
                        }
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            string requestId = "1";
            string agentId = "700000";
            string code = "15666789";

            //testDeposit(requestId, agentId, code);
            testWithdraw(requestId, agentId, code);
        }
    }
}
