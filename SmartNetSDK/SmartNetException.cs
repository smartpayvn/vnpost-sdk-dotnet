﻿using System;
namespace SmartNet
{
    public class SmartNetException : Exception
    {
        public SmartNetException()
        {
        }

        public SmartNetException(string message) : base(message)
        {
        }
    }
}
